<?php declare(strict_types=1);

/**
 * INIT APP
 */

Explore::setPath('D:/Http/');
$mngr = new Manager();

// 1- Config
define('OS',        Config::load()['os']);                                                  // 'Windows'
define('HOST',      Config::load()['host']);                                                // 'anakin.localhost'
define('URL',       Config::load()['url']);                                                 // ''
define('SCHEME',    Config::load()['scheme']);                                              // 'http'
define('DS', (OS === 'Windows') ? DIRECTORY_SEPARATOR == '\\' : DIRECTORY_SEPARATOR);       // '\\'

// 1- Clean URL
$url = $mngr->cleanPath(URL);

// 2- Abs path for site
defined('APP_ROOT_URL') || define('APP_ROOT_URL', SCHEME . '://' . HOST . (!empty($url) ? '/' . $url : ''));
defined('APP_SELF_URL') || define('APP_SELF_URL', SCHEME . '://' . HOST . $_SERVER['PHP_SELF']);

// 3- Clean $root
$root_path = rtrim(Explore::$path, '\\/');
$root_path = str_replace('\\', '/', $root_path);

// 4- Always use ?p=
if (!isset($_GET['p'])) {
	$mngr->redirect(APP_SELF_URL . '?v=g&p=');
}

// 5- Get path
$p = isset($_GET['p']) ? $_GET['p'] : (isset($_POST['p']) ? $_POST['p'] : '');

// 6- Clean path
$p = $mngr->cleanPath($p);

// 7- Define APP_PATH
defined('APP_ROOT_PATH') || define('APP_ROOT_PATH', $root_path);
defined('APP_PATH') || define('APP_PATH', $p);
unset($p, $url, $root_path);

// 8- Get current path
$path = APP_ROOT_PATH;
if (APP_PATH != '') {
	$path .= '/' . APP_PATH;
}

// 9- Get parent path
defined('APP_PARENT') || define('APP_PARENT', $mngr->get_parent_path(APP_PATH));

// 10- Get current path
$current_path = array_slice(explode("/",$path), -1)[0];

// 11- Get Objects
$directories = Explore::getDirectories($path);
$files       = Explore::getFiles($path);

// 12- Render
echo Template::render($directories, $files);

/* ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** */

class Config
{
	public static function load(): array
	{
		return [
			'version'           => '1.0.0',
			'title'             => 'XplorR',
			'os'                => PHP_OS_FAMILY,
			'root'              => $_SERVER['DOCUMENT_ROOT'],
			'host'              => $_SERVER['HTTP_HOST'],
			'scheme'            => $_SERVER['REQUEST_SCHEME'],
			'url'               => '',
			'encoding'          => 'UTF-8',
			'date_format'       => 'd/m/Y H:i',
			'theme'             => 'default',
		];
	}

	public static function navigation(): array
	{
		return [
			['title' => '<i class="fas fa-home fa-3x"></i> <span class="sr-only">(current)</span>', 'url' => './'],
		];
	}

	public static function icon(string $extension): string
	{
		switch ($extension) :
			case 'bmp':
			case 'gif':
			case 'ico':
			case 'jp2':
			case 'jpc':
			case 'jpeg':
			case 'jpg':
			case 'jpx':
			case 'png':
			case 'svg':
			case 'tif':
			case 'tiff':
			case 'wbmp':
			case 'xbm':
				return '<i class="far fa-image"></i>';
				break;
			case 'ai':
			case 'eps':
			case 'fla':
			case 'psd':
			case 'swf':
				return '<i class="far fa-image"></i>';
				break;
			case 'c':
			case 'config':
			case 'cpp':
			case 'cs':
			case 'dtd':
			case 'ftpquota':
			case 'gitignore':
			case 'js':
			case 'json':
			case 'lock':
			case 'map':
			case 'md':
			case 'passwd':
			case 'py':
			case 'sh':
			case 'sql':
			case 'tpl':
			case 'twig':
				return '<i class="far fa-file-code"></i>';
				break;
			case 'php':
			case 'phps':
			case 'phtml':
				return '<i class="far fa-file-code"></i>';
				break;
			case 'conf':
			case 'htaccess':
			case 'ini':
			case 'log':
			case 'txt':
				return '<i class="far fa-file-alt"></i>';
				break;
			case 'css':
			case 'less':
			case 'sass':
			case 'scss':
				return '<i class="fab fa-css3-alt"></i>';
				break;
			case 'htm':
			case 'html':
			case 'shtml':
			case 'xhtml':
				return '<i class="fab fa-html5"></i>';
				break;
			case '7z':
			case 'gz':
			case 'rar':
			case 'tar':
			case 'zip':
				return '<i class="far fa-file-archive"></i>';
				break;
			case 'xml':
			case 'xsl':
				return '<i class="far fa-file-excel"></i>';
				break;
			case 'ods':
			case 'xls':
			case 'xlsx':
				return '<i class="fas fa-file-csv"></i>';
				break;
			case 'csv':
				return '<i class="far fa-file-excel"></i>';
			case 'doc':
			case 'docx':
			case 'odt':
				return '<i class="far fa-file-word"></i>';
				break;
			case 'ppt':
			case 'pptx':
				return '<i class="far fa-file-powerpoint"></i>';
			case 'aac':
			case 'ac3':
			case 'flac':
			case 'm4a':
			case 'mka':
			case 'mp2':
			case 'mp3':
			case 'oga':
			case 'ogg':
			case 'tds':
			case 'wav':
			case 'wma':
				return '<i class="far fa-file-audio"></i>';
				break;
			case 'cue':
			case 'm3u':
			case 'm3u8':
			case 'pls':
				return '<i class="fas fa-headphones-alt"></i>';
				break;
			case '3gp':
			case 'asf':
			case 'avi':
			case 'f4v':
			case 'flv':
			case 'm4v':
			case 'mkv':
			case 'mov':
			case 'mp4':
			case 'mpeg':
			case 'mpg':
			case 'ogm':
			case 'ogv':
			case 'wmv':
				return '<i class="far fa-file-video"></i>';
				break;
			case 'eml':
			case 'msg':
				return '<i class="far fa-envelope"></i>';
				break;
				break;
			case 'bak':
				return '<i class="far fa-clipboard"></i>';
				break;
				break;
			case 'eot':
			case 'fon':
			case 'otf':
			case 'ttc':
			case 'ttf':
			case 'woff':
			case 'woff2':
				return '<i class="fas fa-font"></i>';
				break;
			case 'pdf':
				return '<i class="far fa-file-pdf"></i>';
				break;
			case 'exe':
			case 'msi':
				return '<i class="far fa-file-alt"></i>';
				break;
			case 'bat':
				return '<i class="fas fa-terminal"></i>';
				break;
			default:
				return '<i class="fas fa-info-circle"></i>';
		endswitch;
	}

	public static function whiteListItems(): array
	{
		return [];
	}

	public static function blackListItems(): array
	{
		return [
			'.git',
			'.idea',
			'.htaccess',
			'$RECYCLE.BIN',
			'System Volume Information',
			'var',
			'vendor',
			'template.js',
		];
	}

	public static function admin(): array
	{
		return [
			'karlito15' => password_hash('password', PASSWORD_BCRYPT),
		];
	}
}

class Explore
{
	/**
	 * @var string $path
	 */
	public static $path;

	/**
	 * @param string $path
	 */
	public static function setPath(string $path)
	{
		if ($path === '' || is_null($path)) {
			self::$path = Config::load()['root'];
		}

		self::$path = $path;
	}

	/**
	 * @return array
	 */
	public static function getDirectories(string $path): array
	{
		$blacklist = Config::blackListItems();
		$result    = [];
		$exclude   = [];
		$num       = 1;
		$it        = self::explorer($path);
		$it->rewind();
		while($it->valid()) {
			if ($it->isDir() && !$it->isDot()) {
				if (in_array($it->getBasename(), $blacklist))
					$exclude[] = $it->getBasename();
				else
					$result[] = [
						'order' => $num,
						'name'  => $it->getBasename(),
						'owner' => $it->getOwner(),
						'group' => $it->getGroup(),
						'perms' => $it->getPerms(),
						'date'  => $it->getMTime()
					];
				$num++;
			}

			$it->next();
		}

		if (empty($result)) {
			return [];
		} else {
			return $result;
		}
	}

	/**
	 * @return array
	 */
	public static function getFiles(string $path)
	{
		$blacklist = Config::blackListItems();
		$result    = [];
		$exclude   = [];
		$num       = 1;
		$it        = self::explorer($path);
		$it->rewind();
		while($it->valid()) {
			if ($it->isFile() && !$it->isDot()) :
				if (in_array($it->getBasename(), $blacklist))
					$exclude[] = $it->getBasename();
				else
					$result[] = [
						'order'     => $num,
						'name'      => $it->getBasename(),
						'extension' => $it->getExtension(),
						'owner'     => $it->getOwner(),
						'group'     => $it->getGroup(),
						'perms'     => $it->getPerms(),
						'size'      => $it->getSize(),
						'date'      => $it->getMTime()
					];
				$num++;
			endif;

			$it->next();
		}

		if (empty($result)) {
			return [];
		} else {
			return $result;
		}
	}

	/**
	 * @return DirectoryIterator
	 */
	private static function explorer(string $path): DirectoryIterator
	{
		if (is_dir($path))
			return new DirectoryIterator($path);
		else
			exit('<div class="alert alert-danger">Path not Found !<br>You need to config a root path</div>');
	}
}

class Manager
{
	/**
	 * Clean path
	 * @param string $path
	 * @return string
	 */
	public function cleanPath(string $path, bool $trim = true)
	{
		$path = $trim ? trim($path) : $path;
		$path = trim($path, '\\/');
		$path = str_replace(['../', '..\\'], '', $path);
		$path = $this->get_absolute_path($path);
		if ($path == '..') {
			$path = '';
		}

		return str_replace('\\', '/', $path);
	}

	/**
	 * Path traversal prevention and clean the url
	 * It replaces (consecutive) occurrences of / and \\ with whatever is in DIRECTORY_SEPARATOR, and processes /. and /.. fine.
	 * @param $path
	 * @return string
	 */
	public function get_absolute_path(string $path)
	{
		$path       = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
		$parts      = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
		$absolutes  = [];
		foreach ($parts as $part) {
			if ('.' == $part) continue;
			if ('..' == $part) {
				array_pop($absolutes);
			} else {
				$absolutes[] = $part;
			}
		}
		return implode(DIRECTORY_SEPARATOR, $absolutes);
	}

	/**
	 * Get parent path
	 * @param string $path
	 * @return bool|string
	 */
	public function get_parent_path(string $path)
	{
		$path = $this->cleanPath($path);
		if ($path != '') {
			$array = explode('/', $path);
			if (count($array) > 1) {
				$array = array_slice($array, 0, -1);
				return implode('/', $array);
			}
			return '';
		}
		return false;
	}

	/**
	 * HTTP Redirect
	 * @param string $url
	 * @param int $code
	 */
	function redirect(string $url, int $code = 302)
	{
		header('Location: ' . $url, true, $code);
		exit;
	}
}

class Template
{
	public static function render(array $directories, array $files): string
	{
		self::init();
		$html = '';
		$html .= self::renderBegin();
		$html .= self::renderList($directories, $files);
		$html .= self::renderGrid($directories, $files);
		$html .= self::renderEnd();

		return $html;
	}

	public static function init()
	{
		session_cache_limiter('nocache');
		session_cache_expire(1);
		session_name(Config::load()['title']);
		session_start();
	}

	private static function FileSizeConvert($bytes): string
	{
		switch ($bytes) :
			case 0 || null:
				return '0 B';
				break;
			default:
				$bytes = floatval($bytes);
				$arBytes = [
					0 => [
						"UNIT" => "TB",
						"VALUE" => pow(1024, 4)
					],
					1 => [
						"UNIT" => "GB",
						"VALUE" => pow(1024, 3)
					],
					2 => [
						"UNIT" => "MB",
						"VALUE" => pow(1024, 2)
					],
					3 => [
						"UNIT" => "KB",
						"VALUE" => 1024
					],
					4 => [
						"UNIT" => "B",
						"VALUE" => 1
					],
				];

				foreach($arBytes as $arItem)
				{
					if($bytes >= $arItem["VALUE"])
					{
						$result = $bytes / $arItem["VALUE"];
						$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
						break;
					}
				}

				return $result;
				break;
		endswitch;
	}

	private static function DateConvert($date)
	{
		$config = Config::load();
		$result = new DateTime();
		$result->setTimestamp($date);
		return $result->format($config['date_format']);
	}

	private static function FormatNumber(int $number): string
	{
		return str_pad((string) $number, 2, '0', STR_PAD_LEFT);
	}

	private static function renderBegin(): string
	{
		$title       = APP_PATH;
		$homePage    = 'index.php';
		$currentPage = $_SERVER['REQUEST_URI'];
		if (APP_PATH === '') {
			$title = "Home";
		}
		$parent           = (APP_PATH) ? '<li><a href="index.php?v=' . $_GET['v'] . '&p=' . APP_PARENT . '"><i class="zmdi zmdi-chevron-left"></i><span>Back Parent</span></a></li>' : null;
		$parentBreadcrumb = (APP_PATH) ? '<li class="breadcrumb-item"><a href="index.php?v=' . $_GET['v'] . '&p=' . APP_PARENT . '">' . APP_PARENT . '</a></li>' : null;
		$grid = ($_GET['v'] === 'g') ? ' active' : null;
		$list = ($_GET['v'] === 'l') ? ' active' : null;
		return <<<HTML
<!DOCTYPE HTML>
<html class="no-js" lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
		<title>Xplorer :: Karlito Web</title>
		<!-- Font Icon -->
		<link rel="stylesheet" href="https://cdn-icon.giancarlo-palumbo.ovh/font-awesome/5.13.1/css/all.min.css">
		<link rel="stylesheet" href="https://cdn-template.giancarlo-palumbo.ovh/2020/aero/assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdn-template.giancarlo-palumbo.ovh/2020/aero/assets/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
		<link rel="stylesheet" href="https://cdn-template.giancarlo-palumbo.ovh/2020/aero/assets/plugins/footable-bootstrap/css/footable.standalone.min.css">
		<!-- Custom Css -->
		<link rel="stylesheet" href="https://cdn-template.giancarlo-palumbo.ovh/2020/aero/assets/css/style.min.css">
		<!-- Favicon-->
		<!-- WEB -->
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/favicon-16x16.png" rel="icon" type="image/png" sizes="16x16">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/favicon-32x32.png" rel="icon" type="image/png" sizes="32x32">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/favicon-96x96.png" rel="icon" type="image/png" sizes="96x96">
		<!-- IOS -->
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/apple-icon-precomposed.png" rel="apple-touch-icon-precomposed" sizes="192x192">
		<!-- ANDROID -->
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/android-icon-192x192.png" rel="icon" type="image/png" sizes="192x192">
		<link href="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/manifest.json" rel="manifest">
		<!-- WINDOWS -->
		<meta name="application-name" content="Karlito Web">
		<meta name="msapplication-TileImage" content="https://cdn-favicon.giancarlo-palumbo.ovh/KW-red/ms-icon-144x144.png">
		<meta name="msapplication-TileColor" content="#ECF0F1">
		<style>
			body {
				min-height: 50rem;
			}
			code {
				font-size: 80%;
			}
			.navbar-brand a svg {
				max-height: 30px;
				max-width: 190px;
			}
		</style>
	</head>
	<body class="theme-dark theme-orange">
		<!-- Page Loader -->
		<div class="page-loader-wrapper">
			<div class="loader">
				<div class="m-t-30"><img class="zmdi-hc-spin" src="https://cdn-template.giancarlo-palumbo.ovh/2020/aero/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
				<p>Please wait...</p>
			</div>
		</div>
		<!-- Overlay For Sidebars -->
		<div class="overlay"></div>
		<!-- Main Search -->
		<div id="search">
			<button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
			<form>
				<input type="search" value="" placeholder="Search..." />
				<button type="submit" class="btn btn-primary">Search</button>
			</form>
		</div>
		<!-- Right Icon menu Sidebar -->
		<!-- Left Sidebar -->
		<aside id="leftsidebar" class="sidebar">
			<div class="navbar-brand">
				<button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
				<a href="index.php">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" preserveAspectRatio="xMidYMid meet" x="0px" y="0px" width="1720px" height="260px" viewBox="0 0 1720 260" enable-background="new 0 0 1720 260">
						<g>
							<g id="Logo">
								<path fill="#DCDCDC" d="M43.155,41.428c-4.442,0-8.301,1.601-11.563,4.792c-3.266,3.196-4.897,7.016-4.897,11.463v0.202v146.899 c0,4.585,1.631,8.475,4.897,11.666c3.262,3.196,7.121,4.787,11.563,4.787v8.13c-4.866,0-9.411-0.902-13.649-2.706 c-4.235-1.808-7.953-4.311-11.148-7.501c-3.195-3.196-5.698-6.879-7.499-11.043c-1.807-4.164-2.708-8.688-2.708-13.545V68.097 c0-4.863,0.902-9.412,2.708-13.646c1.801-4.239,4.304-7.953,7.499-11.149c3.195-3.191,6.913-5.694,11.148-7.502 c4.238-1.803,8.783-2.705,13.649-2.705V41.428z" />
								<path fill="#DCDCDC" d="M236.972,221.034c4.447,0,8.308-1.601,11.57-4.791c3.267-3.196,4.898-7.017,4.898-11.459v-0.213V57.683 c0-4.584-1.632-8.479-4.898-11.676c-3.262-3.191-7.123-4.792-11.57-4.792v-8.12c4.863,0,9.411,0.902,13.652,2.705 c4.24,1.808,7.948,4.311,11.148,7.502c3.192,3.196,5.694,6.91,7.502,11.149c1.803,4.235,2.705,8.717,2.705,13.434V194.37 c0,4.853-0.902,9.4-2.705,13.641c-1.808,4.24-4.31,7.953-7.502,11.149c-3.201,3.19-6.909,5.693-11.148,7.501 c-4.241,1.804-8.789,2.706-13.652,2.706V221.034z" />
								<path fill="#C8201B" d="M90.849,112.896v-0.212V51.64c0-2.781,1.004-5.177,3.019-7.188c2.017-2.011,4.486-3.024,7.399-3.024v-8.333H90.849c-5.14,0-9.516,1.808-13.128,5.415c-3.611,3.612-5.415,7.988-5.415,13.13v61.256c0,1.393-0.074,2.918-0.208,4.579c-0.14,1.672-0.521,3.197-1.146,4.589c-0.627,1.393-1.667,2.573-3.125,3.541c-1.459,0.973-3.442,1.459-5.94,1.459v0.081H40.09v8.238h21.797v0.02c2.635,0,4.653,0.481,6.042,1.453c1.385,0.973,2.396,2.188,3.023,3.648c0.624,1.459,1.005,3.055,1.146,4.792c0.134,1.737,0.208,3.302,0.208,4.686v60.841v0.213v0.202c0.134,5.146,1.979,9.452,5.521,12.922c3.543,3.47,7.882,5.208,13.021,5.208h10.418v-8.13c-2.914,0-5.383-1.004-7.399-3.015c-2.015-2.016-3.019-4.478-3.019-7.4v-60.841v-0.203c0-5.136-1.807-9.519-5.416-13.13c-3.615-3.617-7.99-5.42-13.127-5.42c5.137,0,9.481-1.773,13.021-5.309C88.868,122.374,90.708,118.032,90.849,112.896z" />
								<path fill="#DCDCDC" d="M230.514,220.842h-14.219v-0.091c-1.803,0-3.303-0.593-4.478-1.784c-1.18-1.169-1.773-2.599-1.773-4.265 V33.095H185.25v8.333c1.666,0,3.09,0.587,4.269,1.768c1.18,1.19,1.844,2.608,1.98,4.27v173.286h-28.025 c-4.444,0-8.267-1.601-11.462-4.798c-3.195-3.196-4.792-7.011-4.792-11.463V33.095h-24.793v8.333c1.667,0,3.09,0.587,4.273,1.768 c1.178,1.19,1.836,2.608,1.977,4.27V194.08c0,4.857,0.902,9.407,2.71,13.642c1.803,4.245,4.306,7.959,7.5,11.144 c3.192,3.197,6.874,5.705,11.045,7.508c4.165,1.809,8.676,2.705,13.542,2.705h52.821v-0.005h14.219V220.842z" />
							</g>
							<g id="Karlito">
								<path fill="#DCDCDC" d="M353.476,205.014h-31.514V59.242h31.514v60.813l56.845-60.813h39.086l-60.045,61.82l62.625,83.952h-38.684 l-45.67-62.022l-14.156,14.761V205.014z" />
								<path fill="#DCDCDC" d="M531.204,54.65l79.964,150.363h-34.477l-11.577-22.351h-68.002l-11.157,22.351h-34.702L531.204,54.65z M549.951,155.74l-18.747-38.081l-18.528,38.081H549.951z" />
								<path fill="#DCDCDC" d="M619.819,205.014V59.242h64.783c31.331,0,52.657,20.723,52.657,50.644 c0,21.126-11.559,38.501-29.721,46.275c12.566,15.748,25.917,33.288,37.714,48.854h-37.714 c-11.961-14.962-22.936-29.521-34.897-45.47h-21.527v45.47H619.819z M684.603,132.218c9.767,0,21.345-7.187,21.345-22.333 c0-7.571-2.799-23.337-21.345-23.337h-33.489v45.67H684.603z" />
								<path fill="#DCDCDC" d="M873.094,205.014h-108.07V59.242h31.713v118.446h76.356V205.014z" />
								<path fill="#DCDCDC" d="M896.084,205.014V59.242h31.496v145.772H896.084z" />
								<path fill="#DCDCDC" d="M1021.92,86.548v118.466h-31.679V86.548h-42.487V59.242h116.452v27.306H1021.92z" />
								<path fill="#DCDCDC" d="M1072.821,132.018c0-41.464,35.08-75.576,78.373-75.576c43.476,0,78.774,34.112,78.774,75.576 c0,41.884-35.299,75.794-78.774,75.794C1107.901,207.812,1072.821,173.901,1072.821,132.018z M1104.115,132.018 c0,25.131,21.144,47.261,47.079,47.261c26.118,0,47.06-22.13,47.06-47.261c0-24.912-20.941-46.86-47.06-46.86 C1125.259,85.157,1104.115,107.106,1104.115,132.018z" />
							</g>
							<g id="Web">
								<path fill="#C8201B" d="M1380.28,208.607l-41.669-86.549l-42.086,86.549L1238.29,57.659h33.909l28.111,75.576l38.301-79.38 l38.285,79.38l28.716-75.576h33.507L1380.28,208.607z" />
								<path fill="#C8201B" d="M1489.435,117.284h69.776v27.509h-69.776v31.111h78.958v27.325h-110.472V57.458h110.472v27.307h-78.958 V117.284z" />
								<path fill="#C8201B" d="M1687.296,127.052c12.968,4.372,24.326,18.346,24.326,33.892c0,32.501-24.728,42.286-51.048,42.286 h-63.009V57.458h60.613c28.313,0,47.462,10.571,47.462,40.476C1705.641,111.084,1698.069,122.661,1687.296,127.052z M1658.783,115.291c11.174,0,15.143-8.596,15.143-16.168c0-7.572-6.584-14.357-14.942-14.357h-29.721v30.526H1658.783z M1680.126,159.352c0-8.778-6.584-16.552-20.154-16.552h-30.709v33.104h31.312 C1673.542,175.904,1680.126,167.931,1680.126,159.352z" />
							</g>
						</g>
					</svg>
				</a>
			</div>
			<div class="menu">
				<ul class="list">
					<li>
						<div class="user-info">
							<a class="image" href="index.php"><img src="https://cdn-image.giancarlo-palumbo.ovh/avatars/comics-bat-man-512x512.png" alt="Avatar" /></a>
							<div class="detail">
								<h4>Bruce Wayne</h4>
								<small>Batman</small>
							</div>
						</div>
					</li>
					<li class="active"><a href="index.php"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
					$parent
				</ul>
			</div>
		</aside>
		<!-- Right Sidebar -->
		<!-- Content -->
		<section class="content">
			<div class="body_scroll">
				<div class="block-header">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-sm-12">
							<h2>$title</h2>
							<ul class="breadcrumb mt-2">
								<li class="breadcrumb-item"><a href="$homePage"><i class="zmdi zmdi-home"></i> Home </a></li>
								$parentBreadcrumb
								<li class="breadcrumb-item"><a href="$currentPage">$title</a></li>
							</ul>
							<button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-12">
							<!-- <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button> -->
							<!-- <button class="btn btn-success btn-icon float-right" type="button"><i class="zmdi zmdi-upload"></i></button> -->
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row clearfix">
						<div class="col-lg-12">
							<div class="card">
								<ul class="nav nav-tabs pl-0 pr-0">
									<li class="nav-item"><a class="nav-link$list" data-toggle="tab" href="#list_view">List View</a></li>
									<li class="nav-item"><a class="nav-link$grid" data-toggle="tab" href="#grid_view">Grid View</a></li>
								</ul>
								<div class="tab-content">
HTML;
	}

	private static function renderEnd(): string
	{
		return <<<HTML
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Jquery Core Js -->
		<script src="https://cdn-template.giancarlo-palumbo.ovh/2020/aero/assets/bundles/libscripts.bundle.js"></script>
		<script src="https://cdn-template.giancarlo-palumbo.ovh/2020/aero/assets/bundles/vendorscripts.bundle.js"></script>
		<script src="https://cdn-template.giancarlo-palumbo.ovh/2020/aero/assets/bundles/mainscripts.bundle.js"></script>
		<script src="https://cdn-template.giancarlo-palumbo.ovh/template.js"></script>
	</body>
</html>
HTML;
	}

	private static function renderList(array $directories, array $files)
	{
		$active = ($_GET['v'] === 'l') ? ' active' : null;
		$html = '';
		$html .= '<div class="tab-pane' . $active . '" id="list_view">' . PHP_EOL;
		$html .= '<div class="row clearfix">' . PHP_EOL;
		$html .= "\t" . '<div class="table-responsive">' . PHP_EOL;
		$html .= "\t\t" . '<table class="table mb-0 c_table">' . PHP_EOL;
		$html .= "\t\t\t" . '<thead>' . PHP_EOL;
		$html .= "\t\t\t\t" . '<tr>' . PHP_EOL;
		$html .= "\t\t\t\t\t" . '<th>Name</th>' . PHP_EOL;
		$html .= "\t\t\t\t\t" . '<th data-breakpoints="xs">Owner</th>' . PHP_EOL;
		$html .= "\t\t\t\t\t" . '<th class="text-right" data-breakpoints="xs sm md">Last Modified</th>' . PHP_EOL;
		$html .= "\t\t\t\t\t" . '<th class="text-right" data-breakpoints="xs">File size</th>' . PHP_EOL;
		$html .= "\t\t\t\t" . '</tr>' . PHP_EOL;
		$html .= "\t\t\t" . '</thead>' . PHP_EOL;
		$html .= "\t\t\t" . '<tbody>' . PHP_EOL;
		foreach ($directories as $directory)
		{
			$html .= "\t\t\t\t" . '<tr>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<td><span><i class="zmdi zmdi-folder text-warning w25"></i>' . htmlspecialchars($directory['name'], ENT_QUOTES, 'UTF-8') .'</span></td>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<td><span class="owner">Me</span></td>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<td class="text-right"><span class="date">' . self::DateConvert($directory['date']) .'</span></td>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<td class="text-right"<span class="size">-</span></td>' . PHP_EOL;
			$html .= "\t\t\t\t" . '</tr>' . PHP_EOL;
		}

		foreach ($files as $file)
		{
			$html .= "\t\t\t\t" . '<tr>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<td><span>' . Config::icon($file['extension']) . ' ' . htmlspecialchars($file['name'], ENT_QUOTES, 'UTF-8') .'</span></td>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<td><span class="owner">Me</span></td>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<td class="text-right"><span class="date">' . self::DateConvert($file['date']) .'</span></td>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<td class="text-right"<span class="size">-</span></td>' . PHP_EOL;
			$html .= "\t\t\t\t" . '</tr>' . PHP_EOL;
		}
		$html .= "\t\t\t" . '</tbody>' . PHP_EOL;
		$html .= "\t\t" . '</table>' . PHP_EOL;
		$html .= "\t" . '</div>' . PHP_EOL;
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	private static function renderGrid(array $directories, array $files): string
	{
		$active = ($_GET['v'] === 'g') ? ' active' : null;
		$html = '';
		$html .= '<div class="tab-pane file_manager' . $active . '" id="grid_view">' . PHP_EOL;
		$html .= '<div class="row clearfix">' . PHP_EOL;
		foreach ($directories as $directory)
		{
			$html .= "\t" . '<div class="col-lg-2 col-md-3 col-sm-4">' . PHP_EOL;
			$html .= "\t\t" . '<div class="card">' . PHP_EOL;
			$html .= "\t\t\t" . '<a href="?v=g&p=' . urlencode(trim(APP_PATH . '/' . $directory['name'], '/')) .'" class="file">' . PHP_EOL;
			$html .= "\t\t\t\t" . '<div class="icon">' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<i class="zmdi zmdi-folder text-warning"></i>' . PHP_EOL;
			$html .= "\t\t\t\t" . '</div>' . PHP_EOL;
			$html .= "\t\t\t\t" . '<div class="file-name">' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<p class="m-b-5 text-muted">' . htmlspecialchars($directory['name'], ENT_QUOTES, 'UTF-8') .'</p>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<small><!--Size: 0MB --><span class="date">' . self::DateConvert($directory['date']) .'</span></small>' . PHP_EOL;
			$html .= "\t\t\t\t" . '</div>' . PHP_EOL;
			$html .= "\t\t\t" . '</a>' . PHP_EOL;
			$html .= "\t\t" . '</div>' . PHP_EOL;
			$html .= "\t" . '</div>';
		}
		$html .= '</div>';

		$html .= '<div class="row clearfix">' . PHP_EOL;
		foreach ($files as $file)
		{
			$html .= "\t" . '<div class="col-lg-2 col-md-2 col-sm-4">' . PHP_EOL;
			$html .= "\t\t" . '<div class="card">' . PHP_EOL;
			$html .= "\t\t\t" . '<a href="' . APP_PATH . '/' . $file['name'] . '" class="file" target="_new">' . PHP_EOL;
			$html .= "\t\t\t\t" . '<div class="icon">' . PHP_EOL;
			$html .= "\t\t\t\t\t" . Config::icon($file['extension']) . PHP_EOL;
			$html .= "\t\t\t\t" . '</div>' . PHP_EOL;
			$html .= "\t\t\t\t" . '<div class="file-name">' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<p class="m-b-5 text-muted">' . htmlspecialchars($file['name'], ENT_QUOTES, 'UTF-8') .'</p>' . PHP_EOL;
			$html .= "\t\t\t\t\t" . '<small>Size: ' . self::FileSizeConvert($file['size']) . ' <span class="date">' . self::DateConvert($file['date']) .'</span></small>' . PHP_EOL;
			$html .= "\t\t\t\t" . '</div>' . PHP_EOL;
			$html .= "\t\t\t" . '</a>' . PHP_EOL;
			$html .= "\t\t" . '</div>' . PHP_EOL;
			$html .= "\t" . '</div>';
		}
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}
}
